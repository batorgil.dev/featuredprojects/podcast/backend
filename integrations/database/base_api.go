package database

import (
	"fmt"
	"sync"

	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

func GetModel(db *gorm.DB, model interface{}) (*QueryBuilder, string) {
	db = db.Model(model)
	s, _ := schema.Parse(model, &sync.Map{}, schema.NamingStrategy{})
	return &QueryBuilder{
		DB: db,
	}, s.Table
}

func (db *QueryBuilder) FilterByBase(tableName string, params BaseFilter) *QueryBuilder {
	db = db.BetweenDate(fmt.Sprintf(`"%s".created_at`, tableName), params.CreatedAt)
	db = db.BetweenDate(fmt.Sprintf(`"%s".deleted_at`, tableName), params.DeletedAt)
	return db
}

func (db *QueryBuilder) FilterByBaseModifier(tableName string, params BaseModifierFilter) *QueryBuilder {
	db = db.BetweenDate(fmt.Sprintf(`"%s".updated_at`, tableName), params.UpdatedAt)
	db = db.Equal(fmt.Sprintf(`"%s".creator_id`, tableName), params.CreatorID)
	db = db.Equal(fmt.Sprintf(`"%s".modifier_id`, tableName), params.ModifierID)
	return db
}
