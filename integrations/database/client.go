package database

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func LoadDatabase() {
	db, err := gorm.Open(postgres.Open(fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=disable TimeZone=%s",
		viper.GetString("DB_HOST"),
		viper.GetInt("DB_PORT"),
		viper.GetString("DB_USER"),
		viper.GetString("DB_NAME"),
		viper.GetString("DB_PASSWORD"),
		viper.GetString("DB_TIMEZONE"),
	)), &gorm.Config{
		PrepareStmt:                              true,
		SkipDefaultTransaction:                   true,
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		panic(err.Error())
	}
	db.AutoMigrate(
		// Base
		&databases.Config{},
		&databases.Category{},
		&databases.SocialProvider{},
		// Blog
		&databases.Blog{},
		// Podcast
		&databases.Podcast{},
		&databases.ProviderMap{},
		&databases.PodcastProvider{},
		// User
		&databases.User{},
		&databases.SocialLink{},
		// Customer
		&databases.Review{},
		&databases.Subscription{},
		&databases.Sponser{},
		// Contact us
		&databases.ContactUs{},
	)

	DB = db
}
