---
inject: true
to: api/controllers/<%= path %>.go
skip_if: co.List
after: "Register\\(router fiber.Router\\)"
---
  router.Post("list", co.List).Name("list")
