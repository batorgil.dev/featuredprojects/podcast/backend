// see types of prompts:
// https://github.com/enquirer/enquirer/tree/master/examples
//
module.exports = [
  {
    type: "input",
    name: "path",
    message: "Where you wanted to add?",
  },
  {
    type: "input",
    name: "database_model",
    message: "What is database model name?",
  },
];
