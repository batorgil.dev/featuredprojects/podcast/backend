PGDMP                       |            prodcast    16.1 (Debian 16.1-1.pgdg120+1)     16.1 (Ubuntu 16.1-1.pgdg22.04+1) v    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16384    prodcast    DATABASE     s   CREATE DATABASE prodcast WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';
    DROP DATABASE prodcast;
                app    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                pg_database_owner    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   pg_database_owner    false    4            �            1259    16440    blog_category_map    TABLE     h   CREATE TABLE public.blog_category_map (
    blog_id bigint NOT NULL,
    category_id bigint NOT NULL
);
 %   DROP TABLE public.blog_category_map;
       public         heap    app    false    4            �            1259    16431    blogs    TABLE     �  CREATE TABLE public.blogs (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title text,
    summary text,
    description_html text,
    is_featured boolean,
    cover_path text,
    view_count bigint,
    author_id bigint,
    status text,
    published_at timestamp with time zone
);
    DROP TABLE public.blogs;
       public         heap    app    false    4            �            1259    16430    blogs_id_seq    SEQUENCE     u   CREATE SEQUENCE public.blogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.blogs_id_seq;
       public          app    false    224    4            �           0    0    blogs_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.blogs_id_seq OWNED BY public.blogs.id;
          public          app    false    223            �            1259    16400 
   categories    TABLE     �   CREATE TABLE public.categories (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    name text
);
    DROP TABLE public.categories;
       public         heap    app    false    4            �            1259    16399    categories_id_seq    SEQUENCE     z   CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.categories_id_seq;
       public          app    false    218    4            �           0    0    categories_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;
          public          app    false    217            �            1259    16390    configs    TABLE     �   CREATE TABLE public.configs (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    key text NOT NULL,
    type text NOT NULL,
    value text NOT NULL
);
    DROP TABLE public.configs;
       public         heap    app    false    4            �            1259    16389    configs_id_seq    SEQUENCE     w   CREATE SEQUENCE public.configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.configs_id_seq;
       public          app    false    4    216            �           0    0    configs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.configs_id_seq OWNED BY public.configs.id;
          public          app    false    215            �            1259    16524 
   contact_us    TABLE     �   CREATE TABLE public.contact_us (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    fullname text,
    email text,
    title text,
    message text
);
    DROP TABLE public.contact_us;
       public         heap    app    false    4            �            1259    16523    contact_us_id_seq    SEQUENCE     z   CREATE SEQUENCE public.contact_us_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.contact_us_id_seq;
       public          app    false    4    242            �           0    0    contact_us_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.contact_us_id_seq OWNED BY public.contact_us.id;
          public          app    false    241            �            1259    16455    podcast_category_map    TABLE     n   CREATE TABLE public.podcast_category_map (
    podcast_id bigint NOT NULL,
    category_id bigint NOT NULL
);
 (   DROP TABLE public.podcast_category_map;
       public         heap    app    false    4            �            1259    16461    podcast_providers    TABLE     �   CREATE TABLE public.podcast_providers (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    url text,
    name text,
    logo_path text
);
 %   DROP TABLE public.podcast_providers;
       public         heap    app    false    4            �            1259    16460    podcast_providers_id_seq    SEQUENCE     �   CREATE SEQUENCE public.podcast_providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.podcast_providers_id_seq;
       public          app    false    4    230            �           0    0    podcast_providers_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.podcast_providers_id_seq OWNED BY public.podcast_providers.id;
          public          app    false    229            �            1259    16446    podcasts    TABLE     �  CREATE TABLE public.podcasts (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title text,
    summary text,
    description_html text,
    is_featured boolean,
    audio_path text,
    cover_path text,
    view_count bigint,
    author_id bigint,
    status text,
    published_at timestamp with time zone
);
    DROP TABLE public.podcasts;
       public         heap    app    false    4            �            1259    16445    podcasts_id_seq    SEQUENCE     x   CREATE SEQUENCE public.podcasts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.podcasts_id_seq;
       public          app    false    227    4            �           0    0    podcasts_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.podcasts_id_seq OWNED BY public.podcasts.id;
          public          app    false    226            �            1259    16471    provider_maps    TABLE     �   CREATE TABLE public.provider_maps (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    url text,
    podcast_id bigint,
    podcast_provider_id bigint
);
 !   DROP TABLE public.provider_maps;
       public         heap    app    false    4            �            1259    16470    provider_maps_id_seq    SEQUENCE     }   CREATE SEQUENCE public.provider_maps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.provider_maps_id_seq;
       public          app    false    4    232            �           0    0    provider_maps_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.provider_maps_id_seq OWNED BY public.provider_maps.id;
          public          app    false    231            �            1259    16491    reviews    TABLE     �   CREATE TABLE public.reviews (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    fullname text,
    avatar_path text,
    star bigint,
    comment text
);
    DROP TABLE public.reviews;
       public         heap    app    false    4            �            1259    16490    reviews_id_seq    SEQUENCE     w   CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.reviews_id_seq;
       public          app    false    236    4            �           0    0    reviews_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.reviews_id_seq OWNED BY public.reviews.id;
          public          app    false    235            �            1259    16481    social_links    TABLE     �   CREATE TABLE public.social_links (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    url text,
    user_id bigint,
    social_provider_id bigint
);
     DROP TABLE public.social_links;
       public         heap    app    false    4            �            1259    16480    social_links_id_seq    SEQUENCE     |   CREATE SEQUENCE public.social_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.social_links_id_seq;
       public          app    false    234    4            �           0    0    social_links_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.social_links_id_seq OWNED BY public.social_links.id;
          public          app    false    233            �            1259    16410    social_providers    TABLE     �   CREATE TABLE public.social_providers (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    url text,
    name text,
    logo_path text
);
 $   DROP TABLE public.social_providers;
       public         heap    app    false    4            �            1259    16409    social_providers_id_seq    SEQUENCE     �   CREATE SEQUENCE public.social_providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.social_providers_id_seq;
       public          app    false    4    220            �           0    0    social_providers_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.social_providers_id_seq OWNED BY public.social_providers.id;
          public          app    false    219            �            1259    16511    sponsers    TABLE     �   CREATE TABLE public.sponsers (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    avatar_path text,
    website text,
    logo_path text
);
    DROP TABLE public.sponsers;
       public         heap    app    false    4            �            1259    16510    sponsers_id_seq    SEQUENCE     x   CREATE SEQUENCE public.sponsers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.sponsers_id_seq;
       public          app    false    240    4            �           0    0    sponsers_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.sponsers_id_seq OWNED BY public.sponsers.id;
          public          app    false    239            �            1259    16501    subscriptions    TABLE     �   CREATE TABLE public.subscriptions (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    email text
);
 !   DROP TABLE public.subscriptions;
       public         heap    app    false    4            �            1259    16500    subscriptions_id_seq    SEQUENCE     }   CREATE SEQUENCE public.subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.subscriptions_id_seq;
       public          app    false    4    238            �           0    0    subscriptions_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.subscriptions_id_seq OWNED BY public.subscriptions.id;
          public          app    false    237            �            1259    16420    users    TABLE     w  CREATE TABLE public.users (
    id bigint NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    first_name text NOT NULL,
    last_name text NOT NULL,
    avatar_path text,
    email text NOT NULL,
    "position" text,
    bio text,
    password text,
    is_active boolean,
    role text
);
    DROP TABLE public.users;
       public         heap    app    false    4            �            1259    16419    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          app    false    222    4            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          app    false    221            �           2604    16434    blogs id    DEFAULT     d   ALTER TABLE ONLY public.blogs ALTER COLUMN id SET DEFAULT nextval('public.blogs_id_seq'::regclass);
 7   ALTER TABLE public.blogs ALTER COLUMN id DROP DEFAULT;
       public          app    false    224    223    224            �           2604    16403    categories id    DEFAULT     n   ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);
 <   ALTER TABLE public.categories ALTER COLUMN id DROP DEFAULT;
       public          app    false    217    218    218            �           2604    16393 
   configs id    DEFAULT     h   ALTER TABLE ONLY public.configs ALTER COLUMN id SET DEFAULT nextval('public.configs_id_seq'::regclass);
 9   ALTER TABLE public.configs ALTER COLUMN id DROP DEFAULT;
       public          app    false    215    216    216            �           2604    16527    contact_us id    DEFAULT     n   ALTER TABLE ONLY public.contact_us ALTER COLUMN id SET DEFAULT nextval('public.contact_us_id_seq'::regclass);
 <   ALTER TABLE public.contact_us ALTER COLUMN id DROP DEFAULT;
       public          app    false    242    241    242            �           2604    16464    podcast_providers id    DEFAULT     |   ALTER TABLE ONLY public.podcast_providers ALTER COLUMN id SET DEFAULT nextval('public.podcast_providers_id_seq'::regclass);
 C   ALTER TABLE public.podcast_providers ALTER COLUMN id DROP DEFAULT;
       public          app    false    229    230    230            �           2604    16449    podcasts id    DEFAULT     j   ALTER TABLE ONLY public.podcasts ALTER COLUMN id SET DEFAULT nextval('public.podcasts_id_seq'::regclass);
 :   ALTER TABLE public.podcasts ALTER COLUMN id DROP DEFAULT;
       public          app    false    226    227    227            �           2604    16474    provider_maps id    DEFAULT     t   ALTER TABLE ONLY public.provider_maps ALTER COLUMN id SET DEFAULT nextval('public.provider_maps_id_seq'::regclass);
 ?   ALTER TABLE public.provider_maps ALTER COLUMN id DROP DEFAULT;
       public          app    false    232    231    232            �           2604    16494 
   reviews id    DEFAULT     h   ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq'::regclass);
 9   ALTER TABLE public.reviews ALTER COLUMN id DROP DEFAULT;
       public          app    false    236    235    236            �           2604    16484    social_links id    DEFAULT     r   ALTER TABLE ONLY public.social_links ALTER COLUMN id SET DEFAULT nextval('public.social_links_id_seq'::regclass);
 >   ALTER TABLE public.social_links ALTER COLUMN id DROP DEFAULT;
       public          app    false    233    234    234            �           2604    16413    social_providers id    DEFAULT     z   ALTER TABLE ONLY public.social_providers ALTER COLUMN id SET DEFAULT nextval('public.social_providers_id_seq'::regclass);
 B   ALTER TABLE public.social_providers ALTER COLUMN id DROP DEFAULT;
       public          app    false    219    220    220            �           2604    16514    sponsers id    DEFAULT     j   ALTER TABLE ONLY public.sponsers ALTER COLUMN id SET DEFAULT nextval('public.sponsers_id_seq'::regclass);
 :   ALTER TABLE public.sponsers ALTER COLUMN id DROP DEFAULT;
       public          app    false    239    240    240            �           2604    16504    subscriptions id    DEFAULT     t   ALTER TABLE ONLY public.subscriptions ALTER COLUMN id SET DEFAULT nextval('public.subscriptions_id_seq'::regclass);
 ?   ALTER TABLE public.subscriptions ALTER COLUMN id DROP DEFAULT;
       public          app    false    237    238    238            �           2604    16423    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          app    false    222    221    222            �          0    16440    blog_category_map 
   TABLE DATA           A   COPY public.blog_category_map (blog_id, category_id) FROM stdin;
    public          app    false    225   S�       �          0    16431    blogs 
   TABLE DATA           �   COPY public.blogs (id, created_at, updated_at, deleted_at, title, summary, description_html, is_featured, cover_path, view_count, author_id, status, published_at) FROM stdin;
    public          app    false    224   ��       �          0    16400 
   categories 
   TABLE DATA           R   COPY public.categories (id, created_at, updated_at, deleted_at, name) FROM stdin;
    public          app    false    218   ��       �          0    16390    configs 
   TABLE DATA           [   COPY public.configs (id, created_at, updated_at, deleted_at, key, type, value) FROM stdin;
    public          app    false    216   /�       �          0    16524 
   contact_us 
   TABLE DATA           m   COPY public.contact_us (id, created_at, updated_at, deleted_at, fullname, email, title, message) FROM stdin;
    public          app    false    242   L�       �          0    16455    podcast_category_map 
   TABLE DATA           G   COPY public.podcast_category_map (podcast_id, category_id) FROM stdin;
    public          app    false    228   �       �          0    16461    podcast_providers 
   TABLE DATA           i   COPY public.podcast_providers (id, created_at, updated_at, deleted_at, url, name, logo_path) FROM stdin;
    public          app    false    230   >�       �          0    16446    podcasts 
   TABLE DATA           �   COPY public.podcasts (id, created_at, updated_at, deleted_at, title, summary, description_html, is_featured, audio_path, cover_path, view_count, author_id, status, published_at) FROM stdin;
    public          app    false    227   ��       �          0    16471    provider_maps 
   TABLE DATA           u   COPY public.provider_maps (id, created_at, updated_at, deleted_at, url, podcast_id, podcast_provider_id) FROM stdin;
    public          app    false    232   �       �          0    16491    reviews 
   TABLE DATA           o   COPY public.reviews (id, created_at, updated_at, deleted_at, fullname, avatar_path, star, comment) FROM stdin;
    public          app    false    236   =�       �          0    16481    social_links 
   TABLE DATA           p   COPY public.social_links (id, created_at, updated_at, deleted_at, url, user_id, social_provider_id) FROM stdin;
    public          app    false    234   
�       �          0    16410    social_providers 
   TABLE DATA           h   COPY public.social_providers (id, created_at, updated_at, deleted_at, url, name, logo_path) FROM stdin;
    public          app    false    220   ��       �          0    16511    sponsers 
   TABLE DATA           k   COPY public.sponsers (id, created_at, updated_at, deleted_at, avatar_path, website, logo_path) FROM stdin;
    public          app    false    240   Ҡ       �          0    16501    subscriptions 
   TABLE DATA           V   COPY public.subscriptions (id, created_at, updated_at, deleted_at, email) FROM stdin;
    public          app    false    238   ��       �          0    16420    users 
   TABLE DATA           �   COPY public.users (id, created_at, updated_at, deleted_at, first_name, last_name, avatar_path, email, "position", bio, password, is_active, role) FROM stdin;
    public          app    false    222   B�       �           0    0    blogs_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.blogs_id_seq', 52, true);
          public          app    false    223            �           0    0    categories_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.categories_id_seq', 4, true);
          public          app    false    217            �           0    0    configs_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.configs_id_seq', 1, false);
          public          app    false    215            �           0    0    contact_us_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.contact_us_id_seq', 3, true);
          public          app    false    241            �           0    0    podcast_providers_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.podcast_providers_id_seq', 6, true);
          public          app    false    229            �           0    0    podcasts_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.podcasts_id_seq', 46, true);
          public          app    false    226            �           0    0    provider_maps_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.provider_maps_id_seq', 35, true);
          public          app    false    231            �           0    0    reviews_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.reviews_id_seq', 5, true);
          public          app    false    235            �           0    0    social_links_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.social_links_id_seq', 33, true);
          public          app    false    233            �           0    0    social_providers_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.social_providers_id_seq', 4, true);
          public          app    false    219            �           0    0    sponsers_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.sponsers_id_seq', 8, true);
          public          app    false    239            �           0    0    subscriptions_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.subscriptions_id_seq', 5, true);
          public          app    false    237            �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 2, true);
          public          app    false    221            �           2606    16444 (   blog_category_map blog_category_map_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.blog_category_map
    ADD CONSTRAINT blog_category_map_pkey PRIMARY KEY (blog_id, category_id);
 R   ALTER TABLE ONLY public.blog_category_map DROP CONSTRAINT blog_category_map_pkey;
       public            app    false    225    225            �           2606    16438    blogs blogs_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.blogs
    ADD CONSTRAINT blogs_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.blogs DROP CONSTRAINT blogs_pkey;
       public            app    false    224            �           2606    16407    categories categories_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_pkey;
       public            app    false    218            �           2606    16397    configs configs_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.configs
    ADD CONSTRAINT configs_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.configs DROP CONSTRAINT configs_pkey;
       public            app    false    216                        2606    16531    contact_us contact_us_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.contact_us
    ADD CONSTRAINT contact_us_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.contact_us DROP CONSTRAINT contact_us_pkey;
       public            app    false    242            �           2606    16522    users idx_users_email 
   CONSTRAINT     Q   ALTER TABLE ONLY public.users
    ADD CONSTRAINT idx_users_email UNIQUE (email);
 ?   ALTER TABLE ONLY public.users DROP CONSTRAINT idx_users_email;
       public            app    false    222            �           2606    16459 .   podcast_category_map podcast_category_map_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.podcast_category_map
    ADD CONSTRAINT podcast_category_map_pkey PRIMARY KEY (podcast_id, category_id);
 X   ALTER TABLE ONLY public.podcast_category_map DROP CONSTRAINT podcast_category_map_pkey;
       public            app    false    228    228            �           2606    16468 (   podcast_providers podcast_providers_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.podcast_providers
    ADD CONSTRAINT podcast_providers_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.podcast_providers DROP CONSTRAINT podcast_providers_pkey;
       public            app    false    230            �           2606    16453    podcasts podcasts_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.podcasts
    ADD CONSTRAINT podcasts_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.podcasts DROP CONSTRAINT podcasts_pkey;
       public            app    false    227            �           2606    16478     provider_maps provider_maps_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.provider_maps
    ADD CONSTRAINT provider_maps_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.provider_maps DROP CONSTRAINT provider_maps_pkey;
       public            app    false    232            �           2606    16498    reviews reviews_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.reviews DROP CONSTRAINT reviews_pkey;
       public            app    false    236            �           2606    16488    social_links social_links_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.social_links
    ADD CONSTRAINT social_links_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.social_links DROP CONSTRAINT social_links_pkey;
       public            app    false    234            �           2606    16417 &   social_providers social_providers_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.social_providers
    ADD CONSTRAINT social_providers_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.social_providers DROP CONSTRAINT social_providers_pkey;
       public            app    false    220            �           2606    16518    sponsers sponsers_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.sponsers
    ADD CONSTRAINT sponsers_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.sponsers DROP CONSTRAINT sponsers_pkey;
       public            app    false    240            �           2606    16508     subscriptions subscriptions_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.subscriptions DROP CONSTRAINT subscriptions_pkey;
       public            app    false    238            �           2606    16427    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            app    false    222            �           1259    16428    email    INDEX     ?   CREATE UNIQUE INDEX email ON public.users USING btree (email);
    DROP INDEX public.email;
       public            app    false    222            �           1259    16439    idx_blogs_deleted_at    INDEX     L   CREATE INDEX idx_blogs_deleted_at ON public.blogs USING btree (deleted_at);
 (   DROP INDEX public.idx_blogs_deleted_at;
       public            app    false    224            �           1259    16408    idx_categories_deleted_at    INDEX     V   CREATE INDEX idx_categories_deleted_at ON public.categories USING btree (deleted_at);
 -   DROP INDEX public.idx_categories_deleted_at;
       public            app    false    218            �           1259    16398    idx_configs_deleted_at    INDEX     P   CREATE INDEX idx_configs_deleted_at ON public.configs USING btree (deleted_at);
 *   DROP INDEX public.idx_configs_deleted_at;
       public            app    false    216                       1259    16532    idx_contact_us_deleted_at    INDEX     V   CREATE INDEX idx_contact_us_deleted_at ON public.contact_us USING btree (deleted_at);
 -   DROP INDEX public.idx_contact_us_deleted_at;
       public            app    false    242            �           1259    16469     idx_podcast_providers_deleted_at    INDEX     d   CREATE INDEX idx_podcast_providers_deleted_at ON public.podcast_providers USING btree (deleted_at);
 4   DROP INDEX public.idx_podcast_providers_deleted_at;
       public            app    false    230            �           1259    16454    idx_podcasts_deleted_at    INDEX     R   CREATE INDEX idx_podcasts_deleted_at ON public.podcasts USING btree (deleted_at);
 +   DROP INDEX public.idx_podcasts_deleted_at;
       public            app    false    227            �           1259    16479    idx_provider_maps_deleted_at    INDEX     \   CREATE INDEX idx_provider_maps_deleted_at ON public.provider_maps USING btree (deleted_at);
 0   DROP INDEX public.idx_provider_maps_deleted_at;
       public            app    false    232            �           1259    16499    idx_reviews_deleted_at    INDEX     P   CREATE INDEX idx_reviews_deleted_at ON public.reviews USING btree (deleted_at);
 *   DROP INDEX public.idx_reviews_deleted_at;
       public            app    false    236            �           1259    16489    idx_social_links_deleted_at    INDEX     Z   CREATE INDEX idx_social_links_deleted_at ON public.social_links USING btree (deleted_at);
 /   DROP INDEX public.idx_social_links_deleted_at;
       public            app    false    234            �           1259    16418    idx_social_providers_deleted_at    INDEX     b   CREATE INDEX idx_social_providers_deleted_at ON public.social_providers USING btree (deleted_at);
 3   DROP INDEX public.idx_social_providers_deleted_at;
       public            app    false    220            �           1259    16519    idx_sponsers_deleted_at    INDEX     R   CREATE INDEX idx_sponsers_deleted_at ON public.sponsers USING btree (deleted_at);
 +   DROP INDEX public.idx_sponsers_deleted_at;
       public            app    false    240            �           1259    16509    idx_subscriptions_deleted_at    INDEX     \   CREATE INDEX idx_subscriptions_deleted_at ON public.subscriptions USING btree (deleted_at);
 0   DROP INDEX public.idx_subscriptions_deleted_at;
       public            app    false    238            �           1259    16429    idx_users_deleted_at    INDEX     L   CREATE INDEX idx_users_deleted_at ON public.users USING btree (deleted_at);
 (   DROP INDEX public.idx_users_deleted_at;
       public            app    false    222            �      x�3�4�2�4bc.# �H�1z\\\ 4Kq      �   
  x����n#���k�S��M%�N���C���@��h���(����tHYV��y�<I���q�N  ���4��a����b����Awp��w����w}}ۿ��k�<�B�w���x�܎�Q<����.uP֫�4jf�sS�rj�Ą�1���R�ﺜ)=��u����|�mi����;�q��T�+S�x�.���]�t��T.�V+��ժv�Q�rm|�dWfO�[J�N
�
]��+]�i�;u�J�����j�J�m�X�3yH���UU��6��*��F���ʔ�-��F}���W��a[�ח�<��.좼�2���Ry���]��k�������{3���_9��۔���ח�2��ţK��E���¹.��|�'S�S��B���ԇ�j��8�ڏ~��ky��~���R|�����VU�|�jn6�2.~o�g�g�t	-^�W����`�J�SKi�����j]�(̃)|��(>��ޖmm&�[t�&�]*�����w�Ow�<���f#��ZFV�U�׫�m\pMK[���-1��I�ÿ�F�\-�5��=9��a�����+����v���7rG�m!���N�_�;�_�ځ��x0�}Jg�>V-\�n#�&O8mo��lf�)!�q*�@�q���rtq�aT�>Ѫ����a��xЮ��4�<u�w��� ���J>�8]d���4����%7�6�;�uT�VŮ�96]�!��YgR<V�����ޭk�M�i+M��O��˦�⸰F���"c��5j�U�YXi���6�E�ƅ��2�@��[��q�̻ڜ7Z�s���XYt���Ƅ���/zU��j_������g���V�Б�N����陌����������I�e0Ȝ��g�Ԁ���#��8̣����y=1��/��So�����j��4�<J��N/׫�,�f}�j�q��"���M��-k\���JY�rh�Ҍݵ������ű�ɡ���ͺ�m'YL͆&O�D�>k`�%sW۾K�k:��Yqz���Y�dί��J�Nu33��m�a���O��r𮩡���k,S:*����ޓžޞ, 񝠭|[4���z�jYxb
�Z5��J:xbz�k)��>�gu�v�a��ڸ�yNue�oN�0��Lo��u�_�^�n#��_S�^χȿ����s�,�����ɼ�<N���t��ɧ%�=�X�YK�^6�qx��t�~����[�p荓��v��!h�,�������WFO}{�Q�����1ꥉ2&��L�N�rw|��n_��]{�gIW��M�]�_�\��of�+};�to'7���L]=�����t<������1��b ���zRXY/gg
xq7���z�_������6~J	�B|��>�� �|�����W� d&8 � �p �8H��kp 2�p �8 � $��p 2�p �8 � $��8 ��	�8 � �p ����L�� �p �8 �q0�t�i�� �t��:@� �� d&:@� �t��:H��!:@����:@� �t�R�`��A�a�t��:@� ���:`�dt�k�� �t��:@� ��'�&:@� �t��:H�6JF����:@� �t�R뀝��A�a�t��:@� ���:`�dt�k�� �t��:@� ��+�&:@� �t��:H��!{%��\�D� �t��:@�u�^�� �0�:@� �t��Aj�W2:�5Lt��:@� �t�Z알r�t��:@� ��{%��\�D� �t��:@�u�^�� �0�:@� �t��Aj�W2:�5Lt��:@� �t�Z알r�t��:@� ��{%��\�D� �t��:@�u�^�� �0�:@� �t��Ab��+�&:@� �t��:H��JF����:@� �t�R뀽��A�a�t��:@� ���:`�dt�k�� �t��:@� ��+�&:@� �t��:H��JF����:@� �t�R뀽��A�a�t��:@� ���:`�dt�k�� �t��:@� ��+�&:@� �t��:H��JF����:@� �t��`�^�� �0�:@� �t��Aj�W2:�5Lt��:@� �t�Z알r�t��:@� ��{%��\�D� �t��:@�u�^�� �0�:@� �t��Aj�W2:�5Lt��:@� �t�Z알r�t��:@� ��{%��\�D� �t��:@�u�^�� �0�:@� �t��Aj�W2:�5Lt��:@� �t�XW알r�t��:@� ��{%��\�D� �t��:@�u�^�� �0�:@� �t��Ab|�{�����,u]      �   �   x��͹�0E�:��(�Y���(Ӥ�P�������ł�<�ǝ����g��!2Euhِ�=��ݥ�KY �S�h����7��Ӹ�S��hcPY'�ԫ��q�Ӝr��(�d�m�~���(�I�S�) 6%�BB      �      x������ � �      �   �   x����
1D��+n/�$�k��Tll�,m�n\k"IT�{#��(��� �˲�E_*�����Պ˺���P90�`e�Š��1&�m�|V�M���������h]��X��.�$�����OVt�v��.��+xgWZ�;m�0�fR�J��/t1{�A�g�]V��~��mL$�F)�b�U�      �      x�3�4�2�4�2cc�=... ��      �   <  x���[O�0���S�ɮ/Ǘ	M�[aТ1��U�8��@�d�����XTMQ�:��;�L a�py�u�l&��ط��_�2%5k��ժ��^�*�����:P_,zqNܽ��l
R��jչ���vt��XV��a�)����H�C\��<�(T�<XZ.�]�f��1���=�W1ア��ja��RW���t��=�^��[|zZ�i!��sT��t9m�`p�xH�Y�D��iA�ꨩ��_"�9�"�<c�*΅�]�����(���n�CC�fŶ'?��|1�Y���`;^o�����}/"g�  ���e��Qi�.1��j{e�9���{�BQ΁��u��Ka�CKL=�v$�,�h�J��+��"�J2��)v��e`x���뷎{�L F�H���kQ׳�b!ST�qʶ�e�)2Q��^��T(S_M��l�Ӣ�>iS���N>32֣�&�f����W��hq��/q1��z7�G�����ˋͻ�~{���>�/n+��O�ŗ������h*����E���{�?�7����%%�N� �:��Hl���a`1Xkcl����n��1C�      �   q  x����n�F��|'(�,*$E�6` �#@��EP �!9��9gdVE���^��C�F������Y���K�ٝl��i�I��)IW�r'�E���7q|vv��g�8/���b1���}���||}O��ˏ��5�]m.�7��O�[z�tuwy��X���H?�CGW�7d������c���7�q�i_�ot���V��[w,*�+ݩ�8O�)i����̓v�k睮k��I|e��	��#��rU<�_��ߝ.�1�꒔#�h�;�T�?~���Ɇ�L�̾�C}Z�vfoUӳ5�n
���kζ�ƴ�o��Y�~���	W���J7�ή����W������>��5�;n'�R(�;��ۗw�¶MS�皼�/{u��U���+�}P�ªm[�s�ܶ?��x��� ��������6���iG�q��������G�=4^�6��;�S���d�i=��Ý�uƛ�E���~E��<�"�
�÷1�6����t�=�ӑ
ۄ���bԛ�*�˨֪�}��؄�D����������ȏ��^7޽[*��eGIQ.����h�viT�Y�'i�M�����|�c2]��X͢�,�(�MT�/q�SS�M����l�6� �d���C^W��~Ma�xg�y���q��n���:C6��/����X���5��XkB;�5�ư�k`Mhg��֒��5���\�\K�\���5ε��5���\�\��5�&�3\�k�k\�kB;�5�ƹ6�kpMhg��8�0l פv�kp�s�pMjg��8�0n פv�kp�s�pMjg���R��5���\�\ü\����5�5��5���\�\ü\����5�5��5���\�\ü\����5�5��5���\�\ü\����5�5��5���\�\ü\����5Ƶ	�����p�q�a� �I���������p�q�a� �I���������p�q�a� �I���������p�q�a� �I���������p�q�a� �I����Z�y�&�3\�k�k�7�kR;�5�ƹ�y�&�3\�k�k�7�kR;�5�ƹ�y�&�3\�k�k�7�kR;�5�ƹ�y�&�3\�k�k_�4       �   "  x���Mn1���)�2DIs�� ��N� F�H��}5i�5$QV���O��4��"�[��p$-�C�tg���#� ��Y�q8�������azݾ��������l��Pg��P1N9��u��Bp��(���u��v1�x�U��3���L�\#4�U��(&*�PtWUe;X��8A�������P� (�$�TU��:y3:���>�sjU��h9�Ȭ��j�[�kގ���b���Ԕkя>}	P��b3�Zt�Ŗ����Q;�)բ'-'=�^PS�) �S-�ҧ/XVj���PS����ח\���=TgP�iԋ��P�`^�Ύ�C�lC�^ΪLq@�����Q^�&���6/LH��uxaBʡ�q��i���ئ�h��6b�|���z;M���爊Aqm��~��N����������nzߤ�l��q�����߉�r�{���U֜��-A��:�OS'�=��(n����S^�{�x֚���R�����Ai��8I�Hq [n����p�}Y�pi^�E� �#:�G�l|��RN^�q0���to�      �   �  x�u����(���S�S@ ����U�CrH����E"# ������$���F�t7��˝`BR�)W�˃���6��Lڿ�3�L���Uf0�\bJݕeNryk[�7=���TJ/��F�V@��L��,鸓�Ľ�����6FJ}�Yh��A�C�5�(%�-���s>Ɖ|pg羻T�K��c�������5���z�;��01l�Q��X�]J0���$��M��'�#)�[b:�����B��kN�А���À]ɒ��ѻ	sJ<���9�1���az���1��Kq��T�1�?����;Sc�-g�.���fOt�<�D��b#��hi-�zOe�5:p�[D/�W�#�?$����^>�P�4/[p��M��9U�D4�9@ HhN�X�U8F�����	y,s���O���u��	��?� �<����KZ�Au���X{�Vw`�fuǯ�ޕ���4d7�(n��08��Z�Vu���ku��
����=�+�:����sqN�A��y]���u3�#�9��*��\�R�<zj{�ˈ.�����pKDz�;�g�8c�5��/Ɉ����ښ�.!��5ִW2��O.D�Y-7H�|�w��~p�v.�4��+�pp���q	�h\�};�����'���-7�O3:�"��䰫��f�y��ѧ7���EO;������� ���h      �   g  x���Mr�0���)��& �?:DO�����Ɖ݉5��rLOJ��hw��F�= (�A�f�a�AColoH�������둕&���q�}�w���<�����a�X�u+�e��;+�ٴzR�7�8�ue�њ�����n�Pq�W삠k���*�%��A�&�06��U�ܓS�Zo&x�\��p<���W��{ڏ����m;����e��w
1����c�~M�"�a�������s���t:R���p:��	y�$���۠��^1�cZ�0�Sa�4�������c_�Q���������K�ĉq�f��sAȋ��x�$��]g����$��\gK��'�]Ba{K�nk�b����3�_.w.�T��������;y]h�D�9Z3-��Q���X|N�p�j4���S����h�Q�2U�2�1ʀF��Ɛde��)�J�Vc��䃩�(�J�Vc���;+N7��}]�g�2镗~F3�ֿ�$q�e�D3�Vy�1&6�׫h��+4������E+Xp��Q!�X���N2�Z-*D��c��i�����~荮���4[o7�x$����zei���r���E��4���������      �   A  x���An� е}��+0>@�HU7�f�ȉ�Vs��NӴi�lҗ��(@ 2!�T+ij��DN�0OB<H7���4ƺ�N���8����y�U��p���SU�/Q1�~LǱjc#�Q��h#C',#$͌�ظ��Ǐm��+q'$�5�Io���)Cw!����;���d�k� ��#+�|à ��,D�E@-7�ޫ�Q
ȕ�Z/(ކ����(�s6�6�P��y"���ֆ(El�E%o'��*����`WR��}��@�S�.�]jr������</K������ܗ�fcj�|x��F�U��nxY��Jf��      �   �  x���K�1�5}��G6��݇����D�$�OCXM)^Y�e�W%�6$�!��������
���R�|@�(�t���~>O�v[���{�����<H#�'�r��ӹ�z���%]/�{��`U�����k�G4o!�jJ�'1^t:�����_m{���t<|���i[C(]�^�ؼ�����Ijm1���~�Ro�J���y�`7�ͭ:�z����F���&.�����,�!Yɥ�T��c?أ�7	q���줧��X�ԩ�,�s9ar����E�Rt��#ߊ�SF`o�)��)�u�������mٵJy�-5�RdI���c���)�h���"��@�Ԋ:e���)��ku��N$%�+�)"���V)�O��Iϧ���ڲ���	Ps	9:��k�ҧ�W�����r�      �   �   x���;
�0����K�^vO=AO�%}�@RC��Sg	Ƅ�O�'�*F��&9�b�"x�g�_��V�~��a�`\/�ܧ�g�
wd7��A��v��sɗ�>��	]P��ȱw�옦���	�@�A}�N��&��C�2�-2����K����ҧʔc�!i`�      �   E  x�m��R�0���S�饵j9vls�&@�m�J�Y�$
��X�Ix�ʁ�K/�J������A�Iѐ� ��qvJc���0~���l����4IHER���ip�ap�p��Y֚�yEC�f4��$ͳ0�g(ʜ�L�,K�m��]g�IE���q���>���>ȝa�`ȠQ��0�B���`��
���r�e����l���*Y����h�[�vAW�4 �X�5�k�ܢu���J�g5�����Bc�_�{ee��\E`�k�U���С��'1;��ɏ�r\�M�=��������!�yᚧF�_m�7��L��/�zGS����l��!Фǐ&$O�,�?�4'q��/�/��k����Ķ����wb�u�b���%�d%hX
��rE1˒���Hl��g���Y0밅��@� ��k���v�`�o|�=���5��dJ�KJ'����ǵ�K��K���T�i��>��3}q/Z{���'x�d�~z�`z��tԙj�dkށ�r�K����R�-_V��}9Y�GOc~XE�����v{����p�0Q�z�H����%     