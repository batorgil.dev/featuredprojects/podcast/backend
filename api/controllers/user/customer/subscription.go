package customer

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type SubscriptionController struct {
	common.Controller
}

func (co SubscriptionController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Delete("delete/:id", co.Delete).Name("delete")
}

// controller start

type (
	SubscriptionPageFilterInput struct {
		database.PaginationInput
		Email     *string   `json:"email"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	Subscription pagination
// @Tags		[User] [Customer] Subscription
// @Param		filter	body		SubscriptionPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Subscription}}
// @Router		/user/customer/subscription/page [post]
func (co SubscriptionController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SubscriptionPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Subscription{}),
	}

	orm = orm.
		Like("email", params.Email).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Subscription{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

// @Summary	Subscription delete
// @Tags		[User] [Customer] Subscription
// @Param		id	path		int	true	"Subscription ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/customer/subscription/delete/:id [delete]
func (co SubscriptionController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.Subscription{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
