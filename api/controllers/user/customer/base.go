package customer

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
	SubscriptionController{Controller: bc}.Register(router.Group("subscription", middlewares.Auth).Name("subscription."))
	ReviewContoller{Controller: bc}.Register(router.Group("review", middlewares.Auth).Name("review."))
	ContactController{Controller: bc}.Register(router.Group("contact", middlewares.Auth).Name("contact."))
}
