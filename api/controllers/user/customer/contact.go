package customer

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type ContactController struct {
	common.Controller
}

func (co ContactController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Delete("delete/:id", co.Delete).Name("delete")
}

// controller start

type (
	ContactFilterInput struct {
		database.PaginationInput
		FullName  *string   `json:"fullname"`
		Email     *string   `json:"email"`
		Title     *string   `json:"title"`
		Message   *string   `json:"message"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	Contact pagination
// @Tags		[User] [Customer] Contact
// @Param		filter	body		ContactFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.ContactUs}}
// @Router		/user/customer/contact/page [post]
func (co ContactController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ContactFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.ContactUs{}),
	}

	orm = orm.
		Like("fullname", params.FullName).
		Like("email", params.Email).
		Like("title", params.Title).
		Like("message", params.Message).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.ContactUs{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

// @Summary	Contact delete
// @Tags		[User] [Customer] Contact
// @Param		id	path		int	true	"Contact ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/customer/contact/delete/:id [delete]
func (co ContactController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.ContactUs{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
