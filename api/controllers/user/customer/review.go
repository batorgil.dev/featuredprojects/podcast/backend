package customer

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type ReviewContoller struct {
	common.Controller
}

func (co ReviewContoller) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	ReviewPageFilterInput struct {
		database.PaginationInput
		Fullname   *string   `json:"fullname"`
		AvatarPath *string   `json:"avatar_path"`
		Comment    *string   `json:"comment"`
		CreatedAt  []*string `json:"created_at"`
	}
)

// @Summary	Review pagination
// @Tags		[User] [Customer] Review
// @Param		filter	body		ReviewPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Review}}
// @Router		/user/customer/review/page [post]
func (co ReviewContoller) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ReviewPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Review{}),
	}

	orm = orm.
		Like("avatar_path", params.Fullname).
		Like("fullname", params.AvatarPath).
		Like("comment", params.Comment).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Review{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type ReviewCreateInput struct {
	Fullname   string `json:"fullname"`
	AvatarPath string `json:"avatar_path"`
	Star       int    `json:"star"`
	Comment    string `json:"comment"`
}

// @Summary	Review create
// @Tags		[User] [Customer] Review
// @Param		input	body		ReviewCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/customer/review/create [post]
func (co ReviewContoller) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ReviewCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.Review{
		Fullname:   params.Fullname,
		AvatarPath: params.AvatarPath,
		Star:       params.Star,
		Comment:    params.Comment,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type ReviewUpdateInput struct {
	Fullname   string `json:"fullname"`
	AvatarPath string `json:"avatar_path"`
	Star       int    `json:"star"`
	Comment    string `json:"comment"`
}

// @Summary	Review edit
// @Tags		[User] [Customer] Review
// @Param		id		path		int					true	"Review ID"
// @Param		input	body		ReviewUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/customer/review/update/:id [put]
func (co ReviewContoller) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ReviewUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.Review{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"fullname":    params.Fullname,
		"avatar_path": params.AvatarPath,
		"star":        params.Star,
		"comment":     params.Comment,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Review delete
// @Tags		[User] [Customer] Review
// @Param		id	path		int	true	"Review ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/customer/review/delete/:id [delete]
func (co ReviewContoller) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.Review{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
