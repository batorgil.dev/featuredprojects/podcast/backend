package user

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type UserController struct {
	common.Controller
}

func (co UserController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
	router.Post("change_password/:id", co.ChangePassword).Name("change_password")
}

// controller start

type (
	UserPageFilterInput struct {
		database.PaginationInput
		FirstName *string             `json:"first_name"`
		LastName  *string             `json:"last_name"`
		Email     *string             `json:"email"`
		Position  *string             `json:"position"`
		Bio       *string             `json:"bio"`
		IsActive  *string             `json:"is_active"`
		Role      *constants.UserRole `json:"role"`
		CreatedAt []*string           `json:"created_at"`
	}
)

// @Summary	User pagination
// @Tags		[User] User
// @Param		filter	body		UserPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.User}}
// @Router		/user/user/page [post]
func (co UserController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params UserPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.User{}),
	}

	orm = orm.
		Like("first_name", params.FirstName).
		Like("last_name", params.LastName).
		Like("email", params.Email).
		Like("position", params.Position).
		Like("bio", params.Bio).
		Equal("is_active", params.IsActive).
		Equal("role", params.Role).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.User{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Preload("SocialLinks").
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type UserCreateInput struct {
	FirstName   string                 `json:"first_name"`
	LastName    string                 `json:"last_name"`
	AvatarPath  string                 `json:"avatar_path"`
	Email       string                 `json:"email"`
	Position    string                 `json:"position"`
	Bio         string                 `json:"bio"`
	SocialLinks []databases.SocialLink `json:"social_links"`
	Password    string                 `json:"password"`
	IsActive    bool                   `json:"is_active"`
	Role        constants.UserRole     `json:"role"`
}

// @Summary	User create
// @Tags		[User] User
// @Param		input	body		UserCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/user/create [post]
func (co UserController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params UserCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	hashPwd, err := utils.GenerateHash(params.Password)
	if err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, errors.New("Failed to generate hash password :"+err.Error()))
	}

	validSocialLinks := []databases.SocialLink{}
	for _, v := range params.SocialLinks {
		if v.URL != "" {
			validSocialLinks = append(validSocialLinks, v)
		}
	}
	if err := database.DB.Create(&databases.User{
		FirstName:   params.FirstName,
		LastName:    params.LastName,
		AvatarPath:  params.AvatarPath,
		Email:       params.Email,
		Position:    params.Position,
		Bio:         params.Bio,
		SocialLinks: validSocialLinks,
		Password:    hashPwd,
		IsActive:    params.IsActive,
		Role:        params.Role,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type UserUpdateInput struct {
	FirstName   string                 `json:"first_name"`
	LastName    string                 `json:"last_name"`
	AvatarPath  string                 `json:"avatar_path"`
	Email       string                 `json:"email"`
	Position    string                 `json:"position"`
	Bio         string                 `json:"bio"`
	SocialLinks []databases.SocialLink `json:"social_links"`
	IsActive    bool                   `json:"is_active"`
	Role        constants.UserRole     `json:"role"`
}

// @Summary	User edit
// @Tags		[User] User
// @Param		id		path		int				true	"User ID"
// @Param		input	body		UserUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/user/update/:id [put]
func (co UserController) Update(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	var params UserUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")

	user := databases.User{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := tx.Model(&user).Updates(map[string]interface{}{
		"first_name":  params.FirstName,
		"last_name":   params.LastName,
		"avatar_path": params.AvatarPath,
		"email":       params.Email,
		"position":    params.Position,
		"bio":         params.Bio,
		"is_active":   params.IsActive,
		"role":        params.Role,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Where("user_id = ?", user.ID).Delete(&databases.SocialLink{}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	for _, socialLink := range params.SocialLinks {
		socialLink.UserID = user.ID
		if socialLink.URL != "" {
			if err := tx.Create(&socialLink).Error; err != nil {
				return co.SetError(c, fiber.StatusInternalServerError, err)
			}
		}
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	User delete
// @Tags		[User] User
// @Param		id	path		int	true	"User ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/user/delete/:id [delete]
func (co UserController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.User{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type ChangePasswordInput struct {
	Password string `json:"password"`
}

// @Summary	User нууц үг өөрчлөх
// @Tags		[User] User
// @Param		id		path		int					true	"User ID"
// @Param		input	body		ChangePasswordInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/change_password/:id [post]
func (co UserController) ChangePassword(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ChangePasswordInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	hashPwd, err := utils.GenerateHash(params.Password)
	if err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, errors.New("Failed to generate hash password :"+err.Error()))
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.User{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"password": hashPwd,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
