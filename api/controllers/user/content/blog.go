package content

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type BlogController struct {
	common.Controller
}

func (co BlogController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Get("get/:id", co.Get).Name("get")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
	router.Put("publish/:id", co.Publish).Name("publish")
	router.Put("back_to_draft/:id", co.BackToDraft).Name("back_to_draft")
}

// controller start

type (
	BlogPageFilterInput struct {
		database.PaginationInput
		Title           *string                  `json:"title"`
		Summary         *string                  `json:"summary"`
		Status          *constants.ContentStatus `json:"status"`
		DescriptionHTML *string                  `json:"description_html"`
		IsFeatured      *string                  `json:"is_featured"`
		AuthorID        *uint                    `json:"author_id"`
		CategoryIDs     []*uint                  `json:"category_ids"`
		CreatedAt       []*string                `json:"created_at"`
		PublishedAt     []*string                `json:"published_at"`
	}
)

//	@Summary	Blog pagination
//	@Tags		[User] [Content] Blog
//	@Param		filter	body		BlogPageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Blog}}
//	@Router		/user/content/blog/page [post]
func (co BlogController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params BlogPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Blog{}),
	}

	orm = orm.
		Like("title", params.Title).
		Like("summary", params.Summary).
		Like("description_html", params.DescriptionHTML).
		Bool("is_featured", params.IsFeatured).
		Equal("author_id", params.AuthorID).
		Equal("status", params.Status).
		BetweenDate("published_at", params.PublishedAt).
		BetweenDate("created_at", params.CreatedAt)

	if len(params.CategoryIDs) > 0 {
		subQuery := database.DB.Table("blog_category_map").Where("category_id IN ?", params.CategoryIDs).Select("blog_id")
		orm = orm.IncludeSubquery("id", subQuery)
	}

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Blog{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Preload("Author").
		Preload("Categories").
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

//	@Summary	Blog detail
//	@Tags		[User] [Content] Blog
//	@Param		id	path		int	true	"Blog ID"
//	@Success	200	{object}	common.BaseResponse{body=databases.Blog}
//	@Router		/user/content/blog/get/:id [get]
func (co BlogController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	var instance databases.Blog
	if err := database.DB.
		Preload("Author").
		Preload("Categories").
		First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(instance)
}

type BlogCreateInput struct {
	Title           string               `json:"title"`
	Summary         string               `json:"summary"`
	DescriptionHTML string               `json:"description_html"`
	IsFeatured      bool                 `json:"is_featured"`
	CoverPath       string               `json:"cover_path"`
	Categories      []databases.Category `json:"categories,omitempty"`
}

//	@Summary	Blog create
//	@Tags		[User] [Content] Blog
//	@Param		input	body		BlogCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/blog/create [post]
func (co BlogController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params BlogCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	auth := middlewares.GetAuth(c)

	if err := database.DB.Create(&databases.Blog{
		Title:           params.Title,
		Summary:         params.Summary,
		DescriptionHTML: params.DescriptionHTML,
		IsFeatured:      params.IsFeatured,
		CoverPath:       params.CoverPath,
		Categories:      params.Categories,
		ViewCount:       0,
		AuthorID:        auth.ID,
		Status:          constants.ContentStatusDraft,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type BlogUpdateInput struct {
	Title           string               `json:"title"`
	Summary         string               `json:"summary"`
	DescriptionHTML string               `json:"description_html"`
	IsFeatured      bool                 `json:"is_featured"`
	CoverPath       string               `json:"cover_path"`
	Categories      []databases.Category `json:"categories,omitempty"`
}

//	@Summary	Blog edit
//	@Tags		[User] [Content] Blog
//	@Param		id		path		int				true	"Blog ID"
//	@Param		input	body		BlogUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/blog/update/:id [put]
func (co BlogController) Update(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	var params BlogUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	blog := databases.Blog{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := tx.Model(&blog).Updates(map[string]interface{}{
		"title":            params.Title,
		"summary":          params.Summary,
		"description_html": params.DescriptionHTML,
		"is_featured":      params.IsFeatured,
		"cover_path":       params.CoverPath,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Model(&blog).Association("Categories").Clear(); err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Model(&blog).Association("Categories").Append(params.Categories); err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	Blog publish
//	@Tags		[User] [Content] Blog
//	@Param		id	path		int	true	"Blog ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/blog/publish/:id [put]
func (co BlogController) Publish(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	blog := databases.Blog{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := database.DB.Model(&blog).Updates(map[string]interface{}{
		"status":       constants.ContentStatusPublished,
		"published_at": time.Now(),
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	Blog move to draft
//	@Tags		[User] [Content] Blog
//	@Param		id	path		int	true	"Blog ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/blog/back_to_draft/:id [put]
func (co BlogController) BackToDraft(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	blog := databases.Blog{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := database.DB.Model(&blog).Updates(map[string]interface{}{
		"status":       constants.ContentStatusDraft,
		"published_at": time.Time{},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	Blog delete
//	@Tags		[User] [Content] Blog
//	@Param		id	path		int	true	"Blog ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/blog/delete/:id [delete]
func (co BlogController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.Blog{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
