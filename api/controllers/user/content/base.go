package content

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
  PodcastController{Controller: bc}.Register(router.Group("podcast" , middlewares.Auth).Name("podcast."))
	BlogController{Controller: bc}.Register(router.Group("blog", middlewares.Auth).Name("blog."))
}
