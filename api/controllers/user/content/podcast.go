package content

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type PodcastController struct {
	common.Controller
}

func (co PodcastController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Get("get/:id", co.Get).Name("get")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
	router.Put("publish/:id", co.Publish).Name("publish")
	router.Put("back_to_draft/:id", co.BackToDraft).Name("back_to_draft")
}

// controller start

type (
	PodcastPageFilterInput struct {
		database.PaginationInput
		Title           *string                  `json:"title"`
		Summary         *string                  `json:"summary"`
		DescriptionHTML *string                  `json:"description_html"`
		IsFeatured      *string                  `json:"is_featured"`
		AuthorID        *uint                    `json:"author_id"`
		Status          *constants.ContentStatus `json:"status"`
		CategoryIDs     []*uint                  `json:"category_ids"`
		CreatedAt       []*string                `json:"created_at"`
		PublishedAt     []*string                `json:"published_at"`
	}
)

//	@Summary	Podcast pagination
//	@Tags		[User] [Content] Podcast
//	@Param		filter	body		PodcastPageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Podcast}}
//	@Router		/user/content/podcast/page [post]
func (co PodcastController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PodcastPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Podcast{}),
	}

	orm = orm.
		Like("title", params.Title).
		Like("summary", params.Summary).
		Like("description_html", params.DescriptionHTML).
		Bool("is_featured", params.IsFeatured).
		Equal("author_id", params.AuthorID).
		Equal("status", params.Status).
		BetweenDate("published_at", params.PublishedAt).
		BetweenDate("created_at", params.CreatedAt)

	if len(params.CategoryIDs) > 0 {
		subQuery := database.DB.Table("podcast_category_map").Where("category_id IN ?", params.CategoryIDs).Select("podcast_id")
		orm = orm.IncludeSubquery("id", subQuery)
	}

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Podcast{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Preload("Author").
		Preload("Categories").
		Preload("ListenOns").
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

//	@Summary	Podcast detail
//	@Tags		[User] [Content] Podcast
//	@Param		id	path		int	true	"Podcast ID"
//	@Success	200	{object}	common.BaseResponse{body=databases.Podcast}
//	@Router		/user/content/podcast/get/:id [get]
func (co PodcastController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	var instance databases.Podcast
	if err := database.DB.
		Preload("Author").
		Preload("Categories").
		Preload("ListenOns.PodcastProvider").
		First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(instance)
}

type PodcastCreateInput struct {
	Title           string `json:"title"`
	Summary         string `json:"summary"`
	DescriptionHTML string `json:"description_html"`
	IsFeatured      bool   `json:"is_featured"`
	AudioPath       string `json:"audio_path"`
	CoverPath       string `json:"cover_path"`
	// references
	AuthorID   uint                    `json:"author_id"`
	ListenOns  []databases.ProviderMap `json:"listen_ons,omitempty"`
	Categories []databases.Category    `json:"categories,omitempty"`
}

//	@Summary	Podcast create
//	@Tags		[User] [Content] Podcast
//	@Param		input	body		PodcastCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/podcast/create [post]
func (co PodcastController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PodcastCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	validListenOns := []databases.ProviderMap{}
	for _, v := range params.ListenOns {
		if v.URL != "" {
			validListenOns = append(validListenOns, v)
		}
	}

	auth := middlewares.GetAuth(c)
	if err := database.DB.Create(&databases.Podcast{
		Title:           params.Title,
		Summary:         params.Summary,
		DescriptionHTML: params.DescriptionHTML,
		IsFeatured:      params.IsFeatured,
		AudioPath:       params.AudioPath,
		CoverPath:       params.CoverPath,
		AuthorID:        utils.IfAssigment(params.AuthorID == 0, auth.ID, params.AuthorID).(uint),
		Status:          constants.ContentStatusDraft,
		ListenOns:       validListenOns,
		Categories:      params.Categories,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type PodcastUpdateInput struct {
	Title           string `json:"title"`
	Summary         string `json:"summary"`
	DescriptionHTML string `json:"description_html"`
	IsFeatured      bool   `json:"is_featured"`
	AudioPath       string `json:"audio_path"`
	CoverPath       string `json:"cover_path"`
	// references
	AuthorID   uint                    `json:"author_id"`
	ListenOns  []databases.ProviderMap `json:"listen_ons,omitempty"`
	Categories []databases.Category    `json:"categories,omitempty"`
}

//	@Summary	Podcast edit
//	@Tags		[User] [Content] Podcast
//	@Param		id		path		int					true	"Podcast ID"
//	@Param		input	body		PodcastUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/podcast/update/:id [put]
func (co PodcastController) Update(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	var params PodcastUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	podcast := databases.Podcast{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}

	updateRecords := map[string]interface{}{
		"title":            params.Title,
		"summary":          params.Summary,
		"description_html": params.DescriptionHTML,
		"is_featured":      params.IsFeatured,
		"audio_path":       params.AudioPath,
		"cover_path":       params.CoverPath,
	}

	if params.AuthorID != 0 {
		updateRecords["author_id"] = params.AuthorID
	}

	if err := tx.Model(&podcast).Updates(updateRecords).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Model(&podcast).Association("Categories").Clear(); err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Model(&podcast).Association("Categories").Append(params.Categories); err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Where("podcast_id = ?", podcast.ID).Delete(&databases.ProviderMap{}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	for _, listenOn := range params.ListenOns {
		if listenOn.URL != "" {
			listenOn.PodcastID = podcast.ID
			if err := tx.Create(&listenOn).Error; err != nil {
				return co.SetError(c, fiber.StatusInternalServerError, err)
			}
		}
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	Podcast publish
//	@Tags		[User] [Content] Podcast
//	@Param		id	path		int	true	"Podcast ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/podcast/publish/:id [put]
func (co PodcastController) Publish(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	podcast := databases.Podcast{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := database.DB.Model(&podcast).Updates(map[string]interface{}{
		"status":       constants.ContentStatusPublished,
		"published_at": time.Now(),
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	Podcast move to draft
//	@Tags		[User] [Content] Podcast
//	@Param		id	path		int	true	"Podcast ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/podcast/back_to_draft/:id [put]
func (co PodcastController) BackToDraft(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	podcast := databases.Podcast{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := database.DB.Model(&podcast).Updates(map[string]interface{}{
		"status":       constants.ContentStatusDraft,
		"published_at": time.Time{},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	Podcast delete
//	@Tags		[User] [Content] Podcast
//	@Param		id	path		int	true	"Podcast ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/content/podcast/delete/:id [delete]
func (co PodcastController) Delete(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := tx.Delete(&databases.Podcast{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Where("podcast_id = ?", utils.Str2Uint(ID)).Delete(&databases.ProviderMap{}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
