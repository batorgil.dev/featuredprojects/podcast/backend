package user

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/user/content"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/user/customer"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/user/settings"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
)

func Register(bc common.Controller, router fiber.Router) {
	AuthController{Controller: bc}.Register(router.Group("auth"))

	// new routers
	customer.Register(bc, router.Group("customer").Name("customer."))
	settings.Register(bc, router.Group("settings").Name("settings."))
	content.Register(bc, router.Group("content").Name("content."))
	// new controllers
	UserController{Controller: bc}.Register(router.Group("user", middlewares.Auth).Name("user."))
	FileController{Controller: bc}.Register(router.Group("file", middlewares.Auth).Name("file."))
}
