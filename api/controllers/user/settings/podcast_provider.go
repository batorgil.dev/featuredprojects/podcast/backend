package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type PodcastProviderController struct {
	common.Controller
}

func (co PodcastProviderController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	PodcastProviderPageFilterInput struct {
		database.PaginationInput
		URL       *string   `json:"url"`
		Name      *string   `json:"name"`
		CreatedAt []*string `json:"created_at"`
	}
)

//	@Summary	PodcastProvider pagination
//	@Tags		[User] [Settings] PodcastProvider
//	@Param		filter	body		PodcastProviderPageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.PodcastProvider}}
//	@Router		/user/settings/podcast_provider/page [post]
func (co PodcastProviderController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PodcastProviderPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.PodcastProvider{}),
	}

	orm = orm.
		Like("name", params.Name).
		Like("url", params.URL).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.PodcastProvider{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type PodcastProviderCreateInput struct {
	URL      string `json:"url"`
	Name     string `json:"name"`
	LogoPath string `json:"logo_path"`
}

//	@Summary	PodcastProvider create
//	@Tags		[User] [Settings] PodcastProvider
//	@Param		input	body		PodcastProviderCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/settings/podcast_provider/create [post]
func (co PodcastProviderController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PodcastProviderCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.PodcastProvider{
		Name:     params.Name,
		URL:      params.URL,
		LogoPath: params.LogoPath,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type PodcastProviderUpdateInput struct {
	URL      string `json:"url"`
	Name     string `json:"name"`
	LogoPath string `json:"logo_path"`
}

//	@Summary	PodcastProvider edit
//	@Tags		[User] [Settings] PodcastProvider
//	@Param		id		path		int							true	"PodcastProvider ID"
//	@Param		input	body		PodcastProviderUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/settings/podcast_provider/update/:id [put]
func (co PodcastProviderController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PodcastProviderUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.PodcastProvider{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"name":      params.Name,
		"url":       params.URL,
		"logo_path": params.LogoPath,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	PodcastProvider delete
//	@Tags		[User] [Settings] PodcastProvider
//	@Param		id	path		int	true	"PodcastProvider ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/settings/podcast_provider/delete/:id [delete]
func (co PodcastProviderController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.PodcastProvider{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
