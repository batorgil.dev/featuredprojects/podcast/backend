package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
  SponserController{Controller: bc}.Register(router.Group("sponser" , middlewares.Auth).Name("sponser."))
	PodcastProviderController{Controller: bc}.Register(router.Group("podcast_provider", middlewares.Auth).Name("podcast_provider."))
	SocialProviderController{Controller: bc}.Register(router.Group("social_provider", middlewares.Auth).Name("social_provider."))
	CategoryController{Controller: bc}.Register(router.Group("category", middlewares.Auth).Name("category."))
	ConfigController{Controller: bc}.Register(router.Group("config", middlewares.Auth).Name("config."))
}
