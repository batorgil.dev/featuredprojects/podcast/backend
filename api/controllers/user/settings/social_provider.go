package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type SocialProviderController struct {
	common.Controller
}

func (co SocialProviderController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	SocialProviderPageFilterInput struct {
		database.PaginationInput
		URL       *string   `json:"url"`
		Name      *string   `json:"name"`
		CreatedAt []*string `json:"created_at"`
	}
)

//	@Summary	SocialProvider pagination
//	@Tags		[User] [Settings] Social_provider
//	@Param		filter	body		SocialProviderPageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.SocialProvider}}
//	@Router		/user/settings/social_provider/page [post]
func (co SocialProviderController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SocialProviderPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.SocialProvider{}),
	}

	orm = orm.
		Like("url", params.URL).
		Like("name", params.Name).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.SocialProvider{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type SocialProviderCreateInput struct {
	URL      string `json:"url"`
	Name     string `json:"name"`
	LogoPath string `json:"logo_path"`
}

//	@Summary	SocialProvider create
//	@Tags		[User] [Settings] Social_provider
//	@Param		input	body		SocialProviderCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/settings/social_provider/create [post]
func (co SocialProviderController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SocialProviderCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.SocialProvider{
		URL:      params.URL,
		Name:     params.Name,
		LogoPath: params.LogoPath,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type SocialProviderUpdateInput struct {
	URL      string `json:"url"`
	Name     string `json:"name"`
	LogoPath string `json:"logo_path"`
}

//	@Summary	SocialProvider edit
//	@Tags		[User] [Settings] Social_provider
//	@Param		id		path		int							true	"SocialProvider ID"
//	@Param		input	body		SocialProviderUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/settings/social_provider/update/:id [put]
func (co SocialProviderController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SocialProviderUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.SocialProvider{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"url":       params.URL,
		"name":      params.Name,
		"logo_path": params.LogoPath,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	SocialProvider delete
//	@Tags		[User] [Settings] Social_provider
//	@Param		id	path		int	true	"SocialProvider ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/user/settings/social_provider/delete/:id [delete]
func (co SocialProviderController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.SocialProvider{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
