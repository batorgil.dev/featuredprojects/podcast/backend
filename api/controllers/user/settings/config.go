package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
)

type ConfigController struct {
	common.Controller
}

func (co ConfigController) Register(router fiber.Router) {
	router.Post("set/:key", co.Set).Name("page")
	router.Get("get/:key", co.Get).Name("get")
}

// controller start

type (
	ConfigInput struct {
		Type  databases.ConfigType `json:"type"`
		Value string               `json:"value"`
	}
)

// @Summary	Config set
// @Tags		[User] [Settings] Config
// @Param		input	body		ConfigInput	false	"Set input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/settings/config/set/{key} [post]
func (co ConfigController) Set(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	key := c.Params("key")

	var params ConfigInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	existing, err := co.GetConfig(key)
	if err != nil {
		// create new one
		if err := database.DB.Model(&databases.Config{}).
			Where("key = ?", key).Updates(map[string]interface{}{
			"type":  params.Type,
			"value": params.Value,
		}).Error; err != nil {
			return co.SetError(c, fiber.StatusBadRequest, err)
		}
	}

	existing.Key = key
	existing.Type = params.Type
	existing.Value = params.Value
	if err := database.DB.Create(&existing).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Config get
// @Tags		[User] [Settings] Config
// @Param		id	path		int	true	"Config ID"
// @Success	200	{object}	common.BaseResponse{body=databases.Config}
// @Router		/user/settings/config/get/{key} [get]
func (co ConfigController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	key := c.Params("key")
	config, _ := co.GetConfig(key)
	return co.SetBody(config.GetValue())
}
