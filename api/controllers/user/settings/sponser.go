package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type SponserController struct {
	common.Controller
}

func (co SponserController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	SponserPageFilterInput struct {
		database.PaginationInput
		Website   *string   `json:"website"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	Sponser pagination
// @Tags		[User] [Settings] Sponser
// @Param		filter	body		SponserPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Sponser}}
// @Router		/user/settings/sponser/page [post]
func (co SponserController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SponserPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Sponser{}),
	}

	orm = orm.
		Like("website", params.Website).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Sponser{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type SponserCreateInput struct {
	LogoPath string `json:"logo_path"`
	Website  string `json:"website"`
}

// @Summary	Sponser create
// @Tags		[User] [Settings] Sponser
// @Param		input	body		SponserCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/settings/sponser/create [post]
func (co SponserController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SponserCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.Sponser{
		LogoPath: params.LogoPath,
		Website:  params.Website,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type SponserUpdateInput struct {
	LogoPath string `json:"logo_path"`
	Website  string `json:"website"`
}

// @Summary	Sponser edit
// @Tags		[User] [Settings] Sponser
// @Param		id		path		int					true	"Sponser ID"
// @Param		input	body		SponserUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/settings/sponser/update/:id [put]
func (co SponserController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SponserUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.Sponser{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"logo_path": params.LogoPath,
		"website":   params.Website,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Sponser delete
// @Tags		[User] [Settings] Sponser
// @Param		id	path		int	true	"Sponser ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/settings/sponser/delete/:id [delete]
func (co SponserController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.Sponser{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
