package user

import (
	"errors"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/middlewares"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/utils"
)

type AuthController struct {
	common.Controller
}

func (co AuthController) Register(router fiber.Router) {
	router.Post("login", co.Login).Name("login")
	router.Post("init", co.Admin).Name("init")
	router.Post("password_change", middlewares.Auth, co.PasswordChange).Name("info_password_change")
	router.Post("update", middlewares.Auth, co.Update).Name("info_update")
	router.Get("info", middlewares.Auth, co.Info).Name("info")                                // info admin
	router.Get("token_refresh", middlewares.Auth, co.TokenRefresh).Name("info_token_refresh") // info user
}

type (
	LoginInput struct {
		Email    string `json:"email" binding:"required"`    // Имэйл
		Password string `json:"password" binding:"required"` // Нууц үг
	}

	UserInfo struct {
		databases.User
	}

	LoginResponse struct {
		Token string   `json:"token"`
		User  UserInfo `json:"user"`
	}
)

// @Summary	User нэвтрэх
// @Tags		[User] Auth
// @Param		input	body		LoginInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=LoginResponse}
// @Router		/user/auth/login [post]
func (co AuthController) Login(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params LoginInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusNotFound, err)
	}

	query := strings.TrimSpace(params.Email)

	orm := database.DB
	orm = orm.Where("email = ?", query)
	var user databases.User
	if err := orm.First(&user).Error; err != nil {
		return co.SetError(c, fiber.StatusNotFound, errors.New("please check your email and password"))
	}

	if err := utils.ComparePassword(user.Password, params.Password); err != nil {
		return co.SetError(c, fiber.StatusNotFound, errors.New("please check your email and password"))
	}

	claims := UserInfo{
		User: user,
	}

	return co.SetBody(LoginResponse{
		Token: utils.GenerateToken(user.ID),
		User:  claims,
	})
}

// @Summary	User хэрэглэгч үүсгэх
// @Tags		[User] Auth
// @Param		auth	body		LoginInput	true	"Auth"
// @Success	200		{object}	common.BaseResponse{body=LoginInput}
// @Router		/user/auth/init [post]
func (co AuthController) Admin(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	var params LoginInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusNotFound, err)
	}

	if result := tx.First(&databases.User{}, 1); result.Error == nil {
		return co.SetError(c, fiber.StatusBadRequest, errors.New("user already exist"))
	}

	hashPwd, err := utils.GenerateHash(params.Password)
	if err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, errors.New("Failed to generate hash password :"+err.Error()))
	}

	if err := tx.Create(&databases.User{
		LastName:   "Admin",
		FirstName:  "Admin",
		Email:      params.Email,
		Password:   hashPwd,
		Role:       constants.UserRoleAdmin,
		IsActive:   true,
		AvatarPath: "",
		Position:   "CTO",
		Bio:        "",
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(LoginInput{
		Email:    params.Email,
		Password: params.Password,
	})
}

// @Summary	Нэвтэрсэн User-ний мэдээлэл авах
// @Tags		[User] Auth
// @Success	200	{object}	common.BaseResponse{body=UserInfo}
// @Router		/user/auth/info [get]
func (co AuthController) Info(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	auth := middlewares.GetAuth(c)

	claims := UserInfo{
		User: *auth,
	}

	return co.SetBody(claims)
}

type PasswordChangeInput struct {
	OldPassword string `json:"old_password"`
	Password    string `json:"password"`
}

// @Summary	Өөрийн нууц үг өөрчлөх
// @Tags		[User] Auth
// @Param		input	body		PasswordChangeInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/auth/password_change [put]
func (co AuthController) PasswordChange(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PasswordChangeInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	hashPwd, err := utils.GenerateHash(params.Password)
	if err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, errors.New("Failed to generate hash password :"+err.Error()))
	}

	auth := middlewares.GetAuth(c)

	if err := utils.ComparePassword(auth.Password, params.OldPassword); err != nil {
		return co.SetError(c, fiber.StatusNotFound, errors.New("Таны нууц үг буруу байна. Та дахин оролдоно уу."))
	}

	if err := database.DB.Model(&databases.User{
		Base: databases.Base{ID: auth.ID},
	}).Updates(map[string]interface{}{
		"password": hashPwd,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type UpdateInput struct {
	FirstName   string                 `json:"first_name"`
	LastName    string                 `json:"last_name"`
	AvatarPath  string                 `json:"avatar_path"`
	Email       string                 `json:"email"`
	Bio         string                 `json:"bio"`
	SocialLinks []databases.SocialLink `json:"social_links,omitempty"`
}

// @Summary	Өөрийн мэдээлэл өөрчлөх
// @Tags		[User] Auth
// @Param		input	body		UpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/auth/update [put]
func (co AuthController) Update(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	var params UpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	auth := middlewares.GetAuth(c)

	user := databases.User{
		Base: databases.Base{ID: auth.ID},
	}
	if err := tx.Model(&user).Updates(map[string]interface{}{
		"first_name":  params.FirstName,
		"last_name":   params.LastName,
		"avatar_path": params.AvatarPath,
		"email":       params.Email,
		"bio":         params.Bio,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	if err := tx.Where("user_id = ?", user.ID).Delete(&databases.SocialLink{}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	for _, socialLink := range params.SocialLinks {
		socialLink.UserID = user.ID
		if err := tx.Create(&socialLink).Error; err != nil {
			return co.SetError(c, fiber.StatusInternalServerError, err)
		}
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Нэвтэрсэн хэрэгэлчийн token сэргээнэ авах
// @Tags		[User] Auth
// @Success	200	{object}	common.BaseResponse{body=UserInfo}
// @Router		/user/auth/token_refresh [get]
func (co AuthController) TokenRefresh(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	auth := middlewares.GetAuth(c)

	claims := LoginResponse{
		Token: utils.GenerateToken(auth.ID),
		User: UserInfo{
			User: *auth,
		},
	}

	return co.SetBody(claims)
}
