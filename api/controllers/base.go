package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/guest"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/user"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	user.Register(bc, router.Group("user").Name("user."))
	guest.Register(bc, router.Group("guest").Name("guest."))
	// new controllers
}
