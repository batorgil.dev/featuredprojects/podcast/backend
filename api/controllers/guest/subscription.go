package guest

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
)

type SubscriptionController struct {
	common.Controller
}

func (co SubscriptionController) Register(group fiber.Router) {
	group.Post("/subscribe", co.Subscribe).Name("subscribe")
	group.Get("/unsubscribe", co.UnSubscribe).Name("unsubscribe")
}

type SubscriptionInput struct {
	Email string `json:"email"`
}

// @Summary	Subscribe
// @Tags		[Guest] Subscription
// @Param		input	body		SubscriptionInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/guest/subscription/subscribe [post]
func (co SubscriptionController) Subscribe(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params SubscriptionInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	email := strings.TrimSpace(strings.ToLower(params.Email))
	if err := database.DB.Where("email = ?", email).First(&databases.Subscription{}).Error; err == nil {
		return co.SetBody(common.SuccessResponse{Success: true})
	}

	if err := database.DB.Create(&databases.Subscription{
		Email: email,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	// send email
	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	UnSubscribe
// @Tags		[Guest] Subscription
// @Param		email	query		string	false	"email"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/guest/subscription/unsubscribe [get]
func (co SubscriptionController) UnSubscribe(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()
	queries := c.Queries()
	email := queries["email"]
	if email != "" {
		if err := database.DB.Where("email = ?", strings.TrimSpace(strings.ToLower(email))).Delete(&databases.Subscription{}).Error; err != nil {
			return co.SetError(c, fiber.StatusBadRequest, err)
		}
	}

	return co.SetBody(common.SuccessResponse{
		Success: true,
	})
}
