package content

import (
	"sort"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
	"gorm.io/gorm"
)

type PodcastController struct {
	common.Controller
}

func (co PodcastController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Get("get/:id", co.Get).Name("get")
}

// controller start

type (
	PodcastPageFilterInput struct {
		database.PaginationInput
		Title           *string   `json:"title"`
		Summary         *string   `json:"summary"`
		DescriptionHTML *string   `json:"description_html"`
		IsFeatured      *string   `json:"is_featured"`
		AuthorID        *uint     `json:"author_id"`
		CategoryIDs     []*uint   `json:"category_ids"`
		CreatedAt       []*string `json:"created_at"`
		PublishedAt     []*string `json:"published_at"`
	}

	PodcastItem struct {
		ID              uint                    `json:"id"`
		Title           string                  `json:"title"`
		Summary         string                  `json:"summary"`
		DescriptionHTML string                  `json:"description_html"`
		IsFeatured      bool                    `json:"is_featured"`
		AudioPath       string                  `json:"audio_path"`
		CoverPath       string                  `json:"cover_path"`
		ViewCount       int64                   `json:"view_count"`
		PublishedAt     time.Time               `json:"published_at"`
		Author          common.TeamMember       `json:"author,omitempty"`
		ListenOns       []common.PodcastAccount `json:"listen_ons,omitempty"`
	}
)

// @Summary	Podcast pagination
// @Tags		[Guest] [Content] Podcast
// @Param		filter	body		PodcastPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]PodcastItem}}
// @Router		/guest/content/podcast/page [post]
func (co PodcastController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params PodcastPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Podcast{}),
	}

	orm = orm.
		Like("title", params.Title).
		Like("summary", params.Summary).
		Like("description_html", params.DescriptionHTML).
		Bool("is_featured", params.IsFeatured).
		Equal("author_id", params.AuthorID).
		Equal("status", constants.ContentStatusPublished).
		BetweenDate("published_at", params.PublishedAt).
		BetweenDate("created_at", params.CreatedAt)

	if len(params.CategoryIDs) > 0 {
		subQuery := database.DB.Table("podcast_category_map").Where("category_id IN ?", params.CategoryIDs).Select("podcast_id")
		orm = orm.IncludeSubquery("id", subQuery)
	}

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	podcasts := []databases.Podcast{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Preload("Author.SocialLinks.SocialProvider", func(db *gorm.DB) *gorm.DB {
			return db.Order("name ASC")
		}).
		Preload("ListenOns.PodcastProvider").
		Find(&podcasts).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	items := []PodcastItem{}
	for _, podcast := range podcasts {
		podcastItem := dbPodcastToPodcastItem(podcast)
		items = append(items, podcastItem)
	}

	result.Items = items
	return co.SetBody(result)
}

// @Summary	Podcast detail
// @Tags		[Guest] [Content] Podcast
// @Param		id	path		int	true	"Podcast ID"
// @Success	200	{object}	common.BaseResponse{body=PodcastItem}
// @Router		/guest/content/podcast/get/:id [get]
func (co PodcastController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	var instance databases.Podcast
	if err := database.DB.
		Preload("Author.SocialLinks.SocialProvider").
		Preload("ListenOns.PodcastProvider").
		First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(dbPodcastToPodcastItem(instance))
}

func dbPodcastToPodcastItem(podcast databases.Podcast) PodcastItem {
	socialLinks := []common.SocialAccount{}
	author := common.TeamMember{}

	if podcast.Author != nil {
		for _, link := range podcast.Author.SocialLinks {
			if link.SocialProvider != nil {
				socialLinks = append(socialLinks, common.SocialAccount{
					URL:      link.URL,
					Name:     link.SocialProvider.Name,
					LogoPath: link.SocialProvider.LogoPath,
				})
			}
		}

		sort.Slice(socialLinks, func(i, j int) bool {
			return socialLinks[i].Name < socialLinks[j].Name
		})
		author.AvatarPath = podcast.Author.AvatarPath
		author.Bio = podcast.Author.Bio
		author.FirstName = podcast.Author.FirstName
		author.LastName = podcast.Author.LastName
		author.Position = podcast.Author.Position
		author.SocialAcccounts = socialLinks
	}
	listenOns := []common.PodcastAccount{}
	for _, listenOn := range podcast.ListenOns {
		if listenOn.PodcastProvider != nil {
			listenOns = append(listenOns, common.PodcastAccount{
				URL:      listenOn.URL,
				Name:     listenOn.PodcastProvider.Name,
				LogoPath: listenOn.PodcastProvider.LogoPath,
			})
		}
	}
	sort.Slice(listenOns, func(i, j int) bool {
		return listenOns[i].Name < listenOns[j].Name
	})

	return PodcastItem{
		ID:              podcast.ID,
		Title:           podcast.Title,
		Summary:         podcast.Summary,
		DescriptionHTML: podcast.DescriptionHTML,
		IsFeatured:      podcast.IsFeatured,
		CoverPath:       podcast.CoverPath,
		ViewCount:       podcast.ViewCount,
		PublishedAt:     podcast.PublishedAt,
		AudioPath:       podcast.AudioPath,
		ListenOns:       listenOns,
		Author:          author,
	}
}
