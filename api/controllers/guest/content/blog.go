package content

import (
	"sort"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
)

type BlogController struct {
	common.Controller
}

func (co BlogController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Get("get/:id", co.Get).Name("get")
}

// controller start

type (
	BlogPageFilterInput struct {
		database.PaginationInput
		Title           *string   `json:"title"`
		Summary         *string   `json:"summary"`
		DescriptionHTML *string   `json:"description_html"`
		IsFeatured      *string   `json:"is_featured"`
		AuthorID        *uint     `json:"author_id"`
		CategoryIDs     []*uint   `json:"category_ids"`
		CreatedAt       []*string `json:"created_at"`
		PublishedAt     []*string `json:"published_at"`
	}

	BlogItem struct {
		ID              uint                 `json:"id"`
		Title           string               `json:"title"`
		Summary         string               `json:"summary"`
		DescriptionHTML string               `json:"description_html"`
		IsFeatured      bool                 `json:"is_featured"`
		CoverPath       string               `json:"cover_path"`
		ViewCount       int64                `json:"view_count"`
		PublishedAt     time.Time            `json:"published_at"`
		Author          common.TeamMember    `json:"author,omitempty"`
		Categories      []databases.Category `gorm:"many2many:blog_category_map" json:"categories,omitempty"`
	}
)

// @Summary	Blog pagination
// @Tags		[Guest] [Content] Blog
// @Param		filter	body		BlogPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]BlogItem}}
// @Router		/guest/content/blog/page [post]
func (co BlogController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params BlogPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Blog{}),
	}

	orm = orm.
		Like("title", params.Title).
		Like("summary", params.Summary).
		Like("description_html", params.DescriptionHTML).
		Bool("is_featured", params.IsFeatured).
		Equal("author_id", params.AuthorID).
		Equal("status", constants.ContentStatusPublished).
		BetweenDate("published_at", params.PublishedAt).
		BetweenDate("created_at", params.CreatedAt)

	if len(params.CategoryIDs) > 0 {
		subQuery := database.DB.Table("blog_category_map").Where("category_id IN ?", params.CategoryIDs).Select("blog_id")
		orm = orm.IncludeSubquery("id", subQuery)
	}

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	blogs := []databases.Blog{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Preload("Author.SocialLinks.SocialProvider").
		Find(&blogs).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	items := []BlogItem{}
	for _, blog := range blogs {
		blogItem := dbBlogToBlogItem(blog)
		items = append(items, blogItem)
	}
	result.Items = items
	return co.SetBody(result)
}

// @Summary	Blog detail
// @Tags		[Guest] [Content] Blog
// @Param		id	path		int	true	"Blog ID"
// @Success	200	{object}	common.BaseResponse{body=BlogItem}
// @Router		/guest/content/blog/get/:id [get]
func (co BlogController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	var instance databases.Blog
	if err := database.DB.
		Preload("Categories").
		Preload("Author.SocialLinks.SocialProvider").
		First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(dbBlogToBlogItem(instance))
}

func dbBlogToBlogItem(blog databases.Blog) BlogItem {
	socialLinks := []common.SocialAccount{}
	author := common.TeamMember{}

	if blog.Author != nil {
		for _, link := range blog.Author.SocialLinks {
			if link.SocialProvider != nil {
				socialLinks = append(socialLinks, common.SocialAccount{
					URL:      link.URL,
					Name:     link.SocialProvider.Name,
					LogoPath: link.SocialProvider.LogoPath,
				})
			}
		}

		sort.Slice(socialLinks, func(i, j int) bool {
			return socialLinks[i].Name < socialLinks[j].Name
		})
		author.AvatarPath = blog.Author.AvatarPath
		author.Bio = blog.Author.Bio
		author.FirstName = blog.Author.FirstName
		author.LastName = blog.Author.LastName
		author.Position = blog.Author.Position
		author.SocialAcccounts = socialLinks
	}

	return BlogItem{
		ID:              blog.ID,
		Title:           blog.Title,
		Summary:         blog.Summary,
		DescriptionHTML: blog.DescriptionHTML,
		IsFeatured:      blog.IsFeatured,
		CoverPath:       blog.CoverPath,
		ViewCount:       blog.ViewCount,
		PublishedAt:     blog.PublishedAt,
		Author:          author,
		Categories:      blog.Categories,
	}
}
