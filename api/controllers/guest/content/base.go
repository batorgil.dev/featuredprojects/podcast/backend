package content

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
	PodcastController{Controller: bc}.Register(router.Group("podcast").Name("podcast."))
	BlogController{Controller: bc}.Register(router.Group("blog").Name("blog."))
}
