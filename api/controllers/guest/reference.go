package guest

import (
	"sort"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
)

type ReferenceController struct {
	common.Controller
}

func (co ReferenceController) Register(group fiber.Router) {
	group.Get("/social_accounts", co.SocialAccounts).Name("social_accounts")
	group.Get("/sponsers", co.Sponsers).Name("sponsers")
	group.Get("/categories", co.Categories).Name("categories")
	group.Get("/podcast_accounts", co.PodcastAccounts).Name("podcast_accounts")
	group.Get("/team", co.Team).Name("team")
	group.Get("/reviews", co.Reviews).Name("reviews")
	group.Get("/statistics", co.Statistics).Name("statistics")
}

// @Summary	SocialAccount list
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=[]common.SocialAccount}
// @Router		/guest/reference/social_accounts [get]
func (co ReferenceController) SocialAccounts(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	items := []common.SocialAccount{}
	database.DB.
		Model(&databases.SocialProvider{}).
		Order("name asc").
		Find(&items)

	return co.SetBody(items)
}

type SponserPublic struct {
	LogoPath string `gorm:"column:logo_path" json:"logo_path"`
	Website  string `gorm:"column:website" json:"website"`
}

// @Summary	SponserPublic list
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=[]SponserPublic}
// @Router		/guest/reference/sponsers [get]
func (co ReferenceController) Sponsers(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	items := []SponserPublic{}
	database.DB.
		Model(&databases.Sponser{}).
		Order("website asc").
		Find(&items)

	return co.SetBody(items)
}

// @Summary	Category list
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=[]databases.Category{}}
// @Router		/guest/reference/categories [get]
func (co ReferenceController) Categories(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	items := []databases.Category{}
	database.DB.
		Model(&databases.Category{}).
		Order("name asc").
		Find(&items)

	return co.SetBody(items)
}

// @Summary	Podcast account list
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=[]common.PodcastAccount}
// @Router		/guest/reference/podcast_accounts [get]
func (co ReferenceController) PodcastAccounts(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	items := []common.PodcastAccount{}
	database.DB.
		Model(&databases.PodcastProvider{}).
		Order("name asc").
		Find(&items)

	return co.SetBody(items)
}

// @Summary	Team list
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=[]common.TeamMember}
// @Router		/guest/reference/team [get]
func (co ReferenceController) Team(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	users := []databases.User{}
	database.DB.
		Model(&databases.User{}).
		Where("is_active = true").
		Order("first_name asc").
		Preload("SocialLinks.SocialProvider").
		Find(&users)

	items := []common.TeamMember{}
	for _, user := range users {
		socialLinks := []common.SocialAccount{}
		for _, link := range user.SocialLinks {
			if link.SocialProvider != nil {
				socialLinks = append(socialLinks, common.SocialAccount{
					URL:      link.URL,
					Name:     link.SocialProvider.Name,
					LogoPath: link.SocialProvider.LogoPath,
				})
			}
		}
		sort.Slice(socialLinks, func(i, j int) bool {
			return socialLinks[i].Name < socialLinks[j].Name
		})
		items = append(items, common.TeamMember{
			FirstName:       user.FirstName,
			LastName:        user.LastName,
			AvatarPath:      user.AvatarPath,
			Position:        user.Position,
			Bio:             user.Bio,
			SocialAcccounts: socialLinks,
		})
	}
	return co.SetBody(items)
}

type ReviewPublic struct {
	Fullname   string `gorm:"column:fullname" json:"fullname"`
	AvatarPath string `gorm:"column:avatar_path" json:"avatar_path"`
	Star       int    `gorm:"column:star" json:"star"`
	Comment    string `gorm:"column:comment" json:"comment"`
}

// @Summary	Review list
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=[]ReviewPublic}
// @Router		/guest/reference/reviews [get]
func (co ReferenceController) Reviews(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	items := []ReviewPublic{}
	database.DB.
		Model(&databases.Review{}).
		Order("fullname asc").
		Find(&items)

	return co.SetBody(items)
}

type StaticticsItem struct {
	TotalPodcast     int64 `json:"total_podcasts"`
	TotalPodcastView int64 `json:"total_podcast_views"`
	TotalBlogView    int64 `json:"total_blog_views"`
	TotalSubscriber  int64 `json:"total_subscribe"`
}

// @Summary	Statictics
// @Tags		[Guest] Reference
// @Success	200	{object}	common.BaseResponse{body=StaticticsItem}
// @Router		/guest/reference/statistics [get]
func (co ReferenceController) Statistics(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	podcastCount := int64(0)
	database.DB.
		Model(&databases.Podcast{}).
		Where("status = ?", constants.ContentStatusPublished).
		Count(&podcastCount)

	podcastView := int64(0)
	database.DB.
		Model(&databases.Podcast{}).
		Where("status = ?", constants.ContentStatusPublished).
		Select("SUM(view_count)").Row().Scan(&podcastView)

	blogView := int64(0)
	database.DB.
		Model(&databases.Blog{}).
		Where("status = ?", constants.ContentStatusPublished).
		Select("SUM(view_count)").Row().Scan(&blogView)

	subscriptionCount := int64(0)
	database.DB.
		Model(&databases.Subscription{}).
		Count(&subscriptionCount)

	return co.SetBody(StaticticsItem{
		TotalPodcast:     podcastCount,
		TotalPodcastView: podcastView,
		TotalBlogView:    blogView,
		TotalSubscriber:  subscriptionCount,
	})
}
