package guest

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/guest/content"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	content.Register(bc, router.Group("content").Name("content."))
	// new controllers
	ReferenceController{Controller: bc}.Register(router.Group("reference").Name("reference."))
	SubscriptionController{Controller: bc}.Register(router.Group("subscription").Name("subscription."))
	ContactController{Controller: bc}.Register(router.Group("contact").Name("contact."))
}
