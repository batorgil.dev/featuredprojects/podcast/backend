package guest

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/integrations/database"
)

type ContactController struct {
	common.Controller
}

func (co ContactController) Register(group fiber.Router) {
	group.Post("/request", co.Request).Name("request")
}

type ContactRequestInput struct {
	FullName string `json:"fullname"`
	Email    string `json:"email"`
	Title    string `json:"title"`
	Message  string `json:"message"`
}

// @Summary	Contact request
// @Tags		[Guest] Contact
// @Param		input	body		ContactRequestInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/guest/contact/request [post]
func (co ContactController) Request(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ContactRequestInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.ContactUs{
		FullName: params.FullName,
		Email:    params.Email,
		Title:    params.Title,
		Message:  params.Message,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	// send email
	return co.SetBody(common.SuccessResponse{Success: true})
}
