package common

type (
	BaseResponse struct {
		Message    string      `json:"message"`
		StatusCode int         `json:"-"`
		Body       interface{} `json:"body"`
	}

	PaginationResponse struct {
		Total int         `json:"total"`
		Items interface{} `json:"items"`
	}

	CursorResponse struct {
		HasNext bool        `json:"has_next"`
		Items   interface{} `json:"items"`
	}

	SuccessResponse struct {
		Success bool `json:"success"`
	}

	SocialAccount struct {
		URL      string `gorm:"column:url" json:"url"`
		Name     string `gorm:"column:name" json:"name"`
		LogoPath string `gorm:"column:logo_path" json:"logo_path"`
	}

	PodcastAccount struct {
		URL      string `gorm:"column:url" json:"url"`
		Name     string `gorm:"column:name" json:"name"`
		LogoPath string `gorm:"column:logo_path" json:"logo_path"`
	}

	TeamMember struct {
		FirstName       string          `json:"first_name"`
		LastName        string          `json:"last_name"`
		AvatarPath      string          `json:"avatar_path"`
		Position        string          `json:"position"`
		Bio             string          `json:"bio"`
		SocialAcccounts []SocialAccount `json:"social_accounts"`
	}
)
