package databases

import (
	"time"

	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
)

type (
	PodcastProvider struct {
		Base
		URL      string `gorm:"column:url" json:"url"`
		Name     string `gorm:"column:name" json:"name"`
		LogoPath string `gorm:"column:logo_path" json:"logo_path"`
	}

	ProviderMap struct {
		Base
		URL string `gorm:"column:url" json:"url"`
		// references
		PodcastID         uint             `gorm:"column:podcast_id" json:"podcast_id"`
		Podcast           *Podcast         `gorm:"foreignKey:PodcastID" json:"podcast,omitempty"`
		PodcastProviderID uint             `gorm:"column:podcast_provider_id" json:"podcast_provider_id"`
		PodcastProvider   *PodcastProvider `gorm:"foreignKey:PodcastProviderID" json:"podcast_provider,omitempty"`
	}

	Podcast struct {
		Base
		Title           string                  `gorm:"column:title" json:"title"`
		Summary         string                  `gorm:"column:summary" json:"summary"`
		DescriptionHTML string                  `gorm:"column:description_html" json:"description_html"`
		IsFeatured      bool                    `gorm:"column:is_featured" json:"is_featured"`
		AudioPath       string                  `gorm:"column:audio_path" json:"audio_path"`
		CoverPath       string                  `gorm:"column:cover_path" json:"cover_path"`
		ViewCount       int64                   `gorm:"column:view_count" json:"view_count"`
		Status          constants.ContentStatus `gorm:"column:status" json:"status"`
		PublishedAt     time.Time               `gorm:"column:published_at" json:"published_at"`
		// references
		AuthorID   uint          `gorm:"column:author_id" json:"author_id"`
		Author     *User         `gorm:"foreignKey:AuthorID" json:"author,omitempty"`
		ListenOns  []ProviderMap `gorm:"foreignKey:PodcastID" json:"listen_ons,omitempty"`
		Categories []Category    `gorm:"many2many:podcast_category_map" json:"categories,omitempty"`
	}
)
