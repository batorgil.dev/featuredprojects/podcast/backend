package databases

import (
	"time"

	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
)

type (
	Blog struct {
		Base
		Title           string                  `gorm:"column:title" json:"title"`
		Summary         string                  `gorm:"column:summary" json:"summary"`
		DescriptionHTML string                  `gorm:"column:description_html" json:"description_html"`
		IsFeatured      bool                    `gorm:"column:is_featured" json:"is_featured"`
		CoverPath       string                  `gorm:"column:cover_path" json:"cover_path"`
		ViewCount       int64                   `gorm:"column:view_count" json:"view_count"`
		Status          constants.ContentStatus `gorm:"column:status" json:"status"`
		PublishedAt     time.Time               `gorm:"column:published_at" json:"published_at"`
		// references
		AuthorID   uint       `gorm:"column:author_id" json:"author_id"`
		Author     *User      `gorm:"foreignKey:AuthorID" json:"author,omitempty"`
		Categories []Category `gorm:"many2many:blog_category_map" json:"categories,omitempty"`
	}
)
