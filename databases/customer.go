package databases

type (
	Review struct {
		Base
		Fullname   string `gorm:"column:fullname" json:"fullname"`
		AvatarPath string `gorm:"column:avatar_path" json:"avatar_path"`
		Star       int    `gorm:"column:star" json:"star"`
		Comment    string `gorm:"column:comment" json:"comment"`
	}

	Subscription struct {
		Base
		Email string `gorm:"column:email" json:"email"`
	}
)
