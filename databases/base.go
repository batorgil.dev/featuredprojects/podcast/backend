package databases

import (
	"encoding/json"
	"time"

	"gorm.io/gorm"
)

type JSON json.RawMessage

type (
	// Base struct
	Base struct {
		ID        uint           `gorm:"primary_key;autoIncrement:true" json:"id"`
		CreatedAt time.Time      `gorm:"autoCreateTime" json:"created_at"`
		UpdatedAt time.Time      `gorm:"autoUpdateTime" json:"updated_at"`
		DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
	}

	Category struct {
		Base
		Name string `gorm:"column:name" json:"name"`
	}

	SocialProvider struct {
		Base
		URL      string `gorm:"column:url" json:"url"`
		Name     string `gorm:"column:name" json:"name"`
		LogoPath string `gorm:"column:logo_path" json:"logo_path"`
	}

	Sponser struct {
		Base
		LogoPath string `gorm:"column:logo_path" json:"logo_path"`
		Website  string `gorm:"column:website" json:"website"`
	}

	ContactUs struct {
		Base
		FullName string `gorm:"column:fullname" json:"fullname"`
		Email    string `gorm:"column:email" json:"email"`
		Title    string `gorm:"column:title" json:"title"`
		Message  string `gorm:"column:message" json:"message"`
	}
)
