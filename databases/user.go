package databases

import (
	"gitlab.com/batorgil.dev/featuredprojects/podcast/backend/constants"
)

type (
	User struct {
		Base
		FirstName  string             `gorm:"column:first_name;not null" json:"first_name"`
		LastName   string             `gorm:"column:last_name;not null" json:"last_name"`
		AvatarPath string             `gorm:"column:avatar_path" json:"avatar_path"`
		Email      string             `gorm:"index:email,unique;not null" json:"email"`
		Position   string             `gorm:"column:position" json:"position"`
		Bio        string             `gorm:"column:bio" json:"bio"`
		Password   string             `gorm:"column:password" json:"-"`
		IsActive   bool               `gorm:"column:is_active" json:"is_active"`
		Role       constants.UserRole `gorm:"column:role" json:"role"`
		// references
		SocialLinks []SocialLink `gorm:"foreignKey:UserID" json:"social_links,omitempty"`
	}

	SocialLink struct {
		Base
		URL string `gorm:"column:url" json:"url"`
		// references
		UserID           uint            `gorm:"column:user_id" json:"user_id"`
		User             *User           `gorm:"foreignKey:UserID" json:"user,omitempty"`
		SocialProviderID uint            `gorm:"column:social_provider_id" json:"social_provider_id"`
		SocialProvider   *SocialProvider `gorm:"foreignKey:SocialProviderID" json:"social_provider,omitempty"`
	}
)
